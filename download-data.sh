function gdrive_download () {
  CONFIRM=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$1" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')
  wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$CONFIRM&id=$1" -O $2
  rm -rf /tmp/cookies.txt
}

gdrive_download 1PQh3f5YuHp7QXB6FVCo3EcBk2Zz_SKwM Fire-Detection/fire1.mp4
gdrive_download 1PK9kNtkpClnL9cqDXBX-vHQdVdp2IJ9L Fire-Detection/fire2.mp4
gdrive_download 13frz9sK5t3VBRmCB6L4N5orQJd-PSVK2 Fire-Detection/output.avi

gdrive_download 1Gyw4czHykoX0kM1PtMl1PKjj1v4LNwOI Pedestrian-Detection/Data/bus1.mp4
gdrive_download 1I2kxgADhVfE6GT0hJsTeFtOm4NMB0gvF Pedestrian-Detection/Data/pedestrians.avi
gdrive_download 1Fn0emXZE6NmZUiudHJUuBI0Sr1Q9zooP Pedestrian-Detection/Data/two_wheeler2.mp4
gdrive_download 1HunzPbERoaMiJvKzuMWosNO3Cbf0IAgV Pedestrian-Detection/Data/video1.avi
gdrive_download 1NEkS_WsaugmWkijtvRJa63gPCrSENj2j Pedestrian-Detection/Data/video.avi

cp Fire-Detection/output.avi Server/angelapp/media/output.avi
cp Fire-Detection/fire1.mp4 Server/angelapp/media/videos/fire1.avi
cp Fire-Detection/fire2.mp4 Server/angelapp/media/videos/fire2.avi

cp Fire-Detection/output.avi Server/angelhack/angelapp/media/output.avi
cp Fire-Detection/fire1.mp4 Server/angelhack/angelapp/media/videos/fire1.avi
cp Fire-Detection/fire2.mp4 Server/angelhack/angelapp/media/videos/fire2.avi

cp Fire-Detection/fire2.mp4 Server/angelapp/modules/classification/Data/video.mp4
cp Fire-Detection/fire2.mp4 Server/angelhack/angelapp/modules/classification/Data/video.mp4

git clone https://github.com/tensorflow/models.git
cp -r models/research/object_detection Pedestrian-Detection/object_detection

gdrive_download 1JZ4dftvTgoooEiwzjPZhsrtum85SjZ1- Pedestrian-Detection/ssd_mobilenet_v1_coco_2017_11_17.tar.gz
gdrive_download 1uaFuW583fXf1DpHHQxMJHYvlJUw_HHb7 Pedestrian-Detection/object_detection/ssd_mobilenet_v1_coco_2017_11_17/frozen_inference_graph.pb

gdrive_download 1jBwBA9L7kcz1j2ZrxCD1xlvRF8x4jnFF Video-Classification/Data/test.mp4
cp Video-Classification/Data/test.mp4 Video-Classification/Backup/test.mp4
